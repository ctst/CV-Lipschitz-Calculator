I want to use this here to thank Samuel Lelièvre for his great help and patience
especially in cleaning the code and writing the examples/tutorial.

If you encounter any errors or problems, please let me know.
Also if you have suggestions or directly additions let me know and we can add
these features.

Feel free to use and edit this programm as you see fit.

Known issues: For some strange reason defining graphs via directories sometimes
brings up errors at the calculation of candidates.