class MarkedGraph(object):
    """
    Class for marked graphs, e.g. elements of Outer Space aka Culler-Vogtmann space.

    This class provides some additional functions, e.g. to find candidates for
    maximal stretching ((topologically) embedded simple curves, figure of eights
    and barbells) and conversion of cycles to elements of fundamental group and back.
    The calculation of the Lipschitz distance comes from the paper
    https://arxiv.org/pdf/0803.0640.pdf

    CONDITIONS:

    The labels of the edges must be elements of a group (to make sense the
    fundamental group, but you can abuse this) or None.

    The graph is directed in the following sense: an edge is oriented from
    the smaller index to the bigger one (i.e. (2, 3, g) means if you go from
    vertex 2 to 3 corresponds g and from 3 to 2 corresponds to g^-).

    None will be converted to the neutral element and at least one label
    must be not None.

    The giving weights function is a map from edges to your weights, where
    an edge is given by the triple ('smaller vertex', 'bigger vertex', 'label').
    Only tested, when vertices of graph are (real) numbers (they should be
    at least comparable/totally ordered).
    """
    # Variables:
    # * _candidates = [[], [], []]
    # * _weights = {}
    # * _graph = Graph()

    # INIT
    def __init__(self, graph=Graph(), weights=None):
        """
        Initialize

        If no weights are given, all edges are given weight 'wi_j_l'
        (edge = (i, j, l) and change the label accordingly) before making it nice
        """
        # setting the standard variables to trivial
        # first entry are simple curves, second are eights, third are barbells
        self._candidates = [[], [], []]
        if weights:
            # since we actually change the graph for computation, use a copy
            self._weights = deepcopy(weights)
        else:
            # self._weights = {edge: 1 for edge in graph.edges()}
            self._weights = {edge: var('w%s_%s_%s' % (edge[0], edge[1],
                    str(edge[2]).replace('*', '').replace('^-1','_inv'))
                    ) for edge in graph.edges()}
        self.setGraph(graph)

    # HELPER CLASSES
    def _getOne(self):
        """
        Return the 1 of the corresponding group for the labels or 't' if there are no edges.

        If first not None edges label is not in a group, an error is raised.
        """
        edges = self._graph.edges()
        for edge in edges:
            if edge[2]:
                return edge[2].parent().one()
        # this should not happen unless it doesn't matter anyway, will check that...
        # soft TODO: maybe turn this into an error or return None?
        print('WARNING: getOne() was called without any edges/not None labels. '
              'Returned None as value.')
        return None

    # GRAPH AND SPANNING TREE
    # The methods to define/get the graph, spanning tree and to make graph nice

    def setGraph(self, graph=Graph()):
        """
        Set the graph to given variable and change None-labels to the neutral element.
        """
        self._graph = graph
        self._convertNone()

    def _convertNone(self):
        """
        Convert all None edges to neutral element.
        """
        one = self._getOne()
        for edge in self._graph.edges():
            if edge[2] == None:
                self._graph.delete_edge((edge[0], edge[1], None))
                self._graph.add_edge(edge[0], edge[1], one)
                self._weights.update(
                    {(edge[0], edge[1], one):
                     self._weights.pop((edge[0], edge[1], None), None)})

    def getGraph(self):
        """
        Return (non-copy) graph on which the element is based
        """
        return self._graph


    def getSpanningTree(self):
        """
        Return subgraph of the edges with neutral element label
        """
        one = self._getOne()
        self._convertNone()
        spanEdges = [e for e in self._graph.edges() if e[2] == one]
        tree = self._graph.subgraph(edges=spanEdges)
        if tree.has_loops() or tree.has_multiple_edges():
            print('WARNING: Spanning tree has loops or multiple edges.'
                  'Check the labeling. This might cause errors later on.')
        return(tree)

    def makeGraphNice(self, graph=None, stretchVariable=None, considerWeights=False):
        """
        Split edges so graph has no loops or multiple edges.

        This is done by introducing the minimal number of vertices on edges midpoints.

        Respects marking (and weights) (new introduced edges have label
        'stretchVariable' and (weight 0) and 'old' edge keeps marking(&weight)).

        WARNING: This method might throw an error, if weights and marking are
        not correctly set, e.g. if edges are indistuingishable. To keep weights,
        set it on True, else it will break the weights!
        """
        if graph is None:
            graph = self._graph
        vertices = graph.vertices()
        if not vertices:
            print 'Warning: Given graph has has no vertices!'
            return
        newVertexNumber = max(vertices) + 1

        for edge in graph.loops():
            start = min([edge[0], edge[1]])
            end = max([edge[0], edge[1]])
            graph.add_vertices([newVertexNumber, newVertexNumber + 1])

            # introduce edges of three and assign and markings
            graph.add_edge(start, newVertexNumber, edge[2])
            graph.add_edge(newVertexNumber, newVertexNumber + 1, stretchVariable)
            graph.add_edge(end,newVertexNumber+1, stretchVariable)

            # weightspart
            if considerWeights:
                self._weights.update({(start, newVertexNumber, edge[2]): self._weights.pop(edge)})
                self._weights.update({(newVertexNumber, newVertexNumber + 1, stretchVariable): 0})
                self._weights.update({(end, newVertexNumber + 1, stretchVariable): 0})
                # print 'updated weights on ', edge
                # print (start, newVertexNumber, edge[2]), ' : ', self._weights.get((start, newVertexNumber, edge[2]))
                # self._weights.pop(edge, None)
                # self._weights.pop(edge)

            # cleanup
            graph.delete_edge(edge)
            newVertexNumber = newVertexNumber + 2

        for edge in graph.multiple_edges():
            start=min([edge[0], edge[1]])
            end=max([edge[0], edge[1]])
            graph.add_vertex(newVertexNumber)

            # introduce edges and assign marking
            graph.add_edge(start, newVertexNumber, edge[2])
            graph.add_edge(newVertexNumber, end, stretchVariable)

            # weightspart
            if considerWeights:
                self._weights.update({(start, newVertexNumber, edge[2]): self._weights.get(edge)})
                self._weights.update({(end, newVertexNumber, stretchVariable): 0})
                self._weights.pop(edge)

            #cleanup
            graph.delete_edge(edge)
            newVertexNumber = newVertexNumber + 1

    # MARKINGS AND WEIGHTS
    # Methods to set and get markings in different ways
    # Every method that changes marking also resets fundamental group.

    def setWeights(self, weights):
        self._weights = weights

    def updateWeights(self, weights):
        self._weights.update(weights)

    def getWeights(self):
        return deepcopy(self._weights)

    def setCentralWeights(self):
        """
        Set all weights to 1 and return copy of the weighting function
        """
        weights = {}
        G = Graph(self._graph)
        for edge in G.edges():
            weights.update({edge: 1})
        self._weights = weights
        return deepcopy(weights)


    # TODO: Reintroduce this function, it was nice!
    # def setDefaultMarking(self, variables=[], group=None, spanningTree=None):
    #     """
    #     Return a copy of this marking.
    #
    #     variables are the markings which will be used for the edges of the
    #     graph minus the spanning Tree (in the order of edges).
    #     If not sufficient are given, variables will be completed by 'x_'i,
    #     where i is the index of the edge in list.
    #     variables[0] will be the marking of the spanning tree (so should
    #     be the neutral element and by default t)
    #     """
    #     marking = {}
    #     gEdges = self._graph.edges()
    #     if spanningTree:
    #         tEdges = spanningTree.edges()
    #     tEdges = self._graph.min_spanning_tree()
    #     if not variables:
    #         variables = ['t']                                     # [var('t')]
    #     for i in range(len(variables), len(gEdges) - len(tEdges) + 1):
    #         variables.append('s_%s' % i)                          #.append(var('s_%s' % i))
    #     i = 1
    #     for edge in gEdges:
    #         if edge in tEdges:
    #             marking.update({edge: variables[0]})
    #         else:
    #             marking.update({edge: variables[i]})
    #             i += 1
    #     self._marking=marking
    #     self.generateFundGroup()
    #     return deepcopy(marking)

    # CANDIDATES
    # This part involves how to get the candidates and simple cycles
    def simple_cycles(self, Gamma=None, non_duplicates=True):
        """
        Return a copylist of all simple cycles in the given graph Gamma as lists vertices

        OPTIONS:

        - non_duplicates=True enforces the list to have unique elements (only important for multiedges).
        """
        if Gamma is None:
            Gamma = self._graph
        G = Graph(Gamma)    # make copy of given graph
        cycleList = []

        multi = []

        if non_duplicates:
            for edge in G.multiple_edges() + G.loops():
                if not [edge[0], edge[1]] in multi:
                    multi.append([edge[0], edge[1]])
        else:
            multi = G.multiple_edges() + G.loops()
        # print multi
        for mult in multi:
            # case of loops
            if mult[0] == mult[1]:
                cycleList.append([mult[0], mult[0]])
            # case of multiple edge
            else:
                cycleList.append([mult[0], mult[1], mult[0]])
        G.allow_loops(False)
        G.allow_multiple_edges(False)
        edges = G.edges()
        for edge in edges:
            G.delete_edge(edge)     # delete starting edge to avoid duplicates
            # add all cycles containing this edge
            cycleList.extend([[edge[0]] + path for path in
                              G.all_paths(edge[1], edge[0])])
        self._candidates[0] = deepcopy(cycleList)
        return cycleList

    def topoCandidates(self, Gamma=None):
        """
        Returns a list of embedded simple cycles, figure of eights and barbells (in index 0,1,2 resp.) as lists of vertices.
        Uses method simple_cycles to determine simple cycles first.
        DEPRECATED. Was formerly the method candidates and is now obsolete. Also has multpiles in list, if graph has multiedges.
        """
        if Gamma is None:
            Gamma = self._graph
        simples = self.simple_cycles(Gamma=Gamma)
        # make a copy, so that in deletion process below we don't loose them
        candidates = [deepcopy(simples)]
        for path in simples:                 # delete last vertex in loops
            path.pop()

        eights = []                     # list of eights
        barbells = []                   # list of barbells

        lvertices = [edge[0] for edge in Gamma.loops()]
        leights = [[vertex, vertex, vertex] for vertex in set(lvertices)
                   if lvertices.count(vertex) >= 2]   # list of eights consisting of 2 loops

        counter = 0
        number_simples = len(simples)
        # print 'Simple paths', simples
        while simples:
            counter += 1
            cyc = simples.pop()
            # print 'use cycle ',counter,'of', number_simples,':', cyc
            for other_cyc in simples:
                # print 'and compare with:',other_cyc
                intersec = list(set(cyc) & set(other_cyc))
                if len(intersec) == 1:             # single point intersection gives eight
                    # print 'Eights at:', cyc, 'and', other_cyc
                    eightA = cyc[cyc.index(intersec[0]):]
                    eightA.extend(cyc[:cyc.index(intersec[0])])
                    eightB = copy(eightA)          # adding other_cyc anti-clockwise
                    i = other_cyc.index(intersec[0])
                    eightA.extend(other_cyc[i:])
                    eightA.extend(other_cyc[:i+1])
                    eightB.extend(other_cyc[i::-1])
                    if i == 0:  # to avoid negative index
                        eightB.extend(other_cyc[::-1])
                    else:
                        eightB.extend(other_cyc[:i-1:-1])
                    eights.append(eightA)
                    eights.append(eightB)
                    # print 'Found eights:', eightA, 'and', eightB
                if len(intersec) == 0:              # no intersection might gives barbells
                    # print 'Finding Barbells at:',cyc,'and',other_cyc
                    length = len(other_cyc)
                    barb = []
                    for vertcyc in cyc:
                        barb = cyc[cyc.index(vertcyc):]
                        barb.extend(cyc[:cyc.index(vertcyc)])
                        for i in range(length):
                            G = Graph(Gamma)
                            G.delete_vertices([vert for vert in cyc if vert != vertcyc])
                            G.delete_vertices([vert for vert in other_cyc if vert != other_cyc[i]])
                            for path in G.all_paths(vertcyc, other_cyc[i]):
                                path.pop()
                                eightA = copy(barb)
                                eightA.extend(path)
                                eightB = copy(eightA)              # adding other_cyc anti-clockwise
                                eightA.extend(other_cyc[i:])
                                eightA.extend(other_cyc[:i+1])
                                eightB.extend(other_cyc[i::-1])
                                if i == 0:
                                    eightB.extend(other_cyc[::-1])
                                else:
                                    eightB.extend(other_cyc[:i-1:-1])
                                eightA.extend(path[::-1])
                                eightB.extend(path[::-1])
                                barbells.extend([eightA,eightB])

        candidates.append(eights)
        candidates.append(barbells)
        self._candidates = deepcopy(candidates)
        return candidates

    def candidates(self, Gamma=None, reevaluate=True):
        """
        Return the candidates as group elements.
        """
        if not reevaluate and self._candidates[0]:
            return self._candidates
        if Gamma is None:
            Gamma = self._graph
        simples = self.simple_cycles(Gamma=Gamma)
        candidates = []
        for cycle in simples:
            candidates.extend(self.path_to_elementList(cycle))
        candidates = [candidates]      # the simple loops
        for path in simples:           # delete last vertex in loops
            path.pop()

        eights = []                     # list of eights
        barbells = []                   # list of barbells

        loops = Gamma.loops()
        for i in range(len(loops)):
            for j in range(i + 1, len(loops)):
                if loops[i][0] == loops[j][0]:
                    eights.append(loops[i][2]*loops[j][2])
                    eights.append(loops[i][2]/loops[j][2])

        number_simples = len(simples)
        while simples:
            cyc = simples.pop()
            for other_cyc in simples:
                intersec = list(set(cyc) & set(other_cyc))
                if len(intersec) == 1:   # single point intersection gives eight
                    firstCyc = cyc[cyc.index(intersec[0]):]
                    firstCyc.extend(cyc[:cyc.index(intersec[0]) + 1])
                    firstCyc = self.path_to_elementList(firstCyc)
                    i = other_cyc.index(intersec[0])
                    secondCyc = (other_cyc[i:])
                    secondCyc.extend(other_cyc[:i+1])
                    secondCyc = self.path_to_elementList(secondCyc)
                    for first in firstCyc:
                        for second in secondCyc:
                            eights.append(first*second)
                            eights.append(first/second)
                    # print 'Found eights:',eightA,'and',eightB
                if len(intersec) == 0: # no intersection might gives barbells
                    # print 'Finding Barbells at:', cyc, 'and', other_cyc
                    length = len(other_cyc)
                    barb = []
                    for vertcyc in cyc:
                        firstCyc = cyc[cyc.index(vertcyc):]
                        firstCyc.extend(cyc[:cyc.index(vertcyc)+1])
                        firstCyc = self.path_to_elementList(firstCyc)
                        for i in range(length):
                            G = Graph(Gamma)
                            G.delete_vertices([vert for vert in cyc if vert != vertcyc])
                            G.delete_vertices([vert for vert in other_cyc if vert != other_cyc[i]])
                            secondCyc = (other_cyc[i:])
                            secondCyc.extend(other_cyc[:i+1])
                            secondCyc = self.path_to_elementList(secondCyc)
                            for path in G.all_paths(vertcyc, other_cyc[i]):
                                middlePart = self.path_to_elementList(path)
                                for first in firstCyc:
                                    for second in secondCyc:
                                        for mid in middlePart:
                                            barbells.append(first*mid*second/mid)
                                            barbells.append(first*mid/second/mid)
        candidates.append(eights)
        candidates.append(barbells)
        self._candidates = deepcopy(candidates)
        return candidates


    def path_to_elementList(self, path):
        """
        Take a path as list of vertices and return a list of words each representing the path.
        """
        self._convertNone()                  # if there were changes in between
        one = self._getOne()
        letters = []
        for i in range(len(path)-1):
            if path[i] < path[i+1]:
                letters.append(self._graph.edge_label(path[i], path[i+1]))
            else:
                letters.append([letter.inverse() for letter in
                                self._graph.edge_label(path[i], path[i+1])])
        words = []
        word = one

        treshHolds = [len(l) for l in letters]
        treshHolds.append(0)
        i = [0] * (len(letters) + 1)
        j = 0
        nextIterate = True
        while nextIterate:
            if (j > 0 and path[j-1] == path[j+1] and
                letters[j][i[j]]*letters[j-1][i[j-1]] == one):
                # don't walk back and forth again
                i[j] += 1
            else:
                word *= letters[j][i[j]]
                j += 1
                if j == len(letters):
                    words.append(word)

            #increase list indices and shorten word back again:
            while i[j] == treshHolds[j]:
                i[j] = 0
                j -= 1
                if j >= 0:
                    word /= letters[j][i[j]]
                    i[j] += 1
                else:
                    nextIterate = False
                    break
        # if the path is a loop or 2 multiedges we didn't sort out the inverse yet.
        # ToDo: Make this part better.
        oldwords = words
        words = []
        for word in oldwords:
            if not (word.inverse() in words or word in words):
                words.append(word)
        return words

    # FUNDAMENTAL GROUP
    # Functions to generate the fundamental group and to translate it to closed paths in the graph and back

    def generateFundGroup(self):
        """
        Return the subgroup generated by the labels.

        If there are no edges, return None.
        """
        generators = self._graph.edge_labels()
        if generators:
            return generators[0].parent().subgroup(generators)
        else:
            return None

    def groupelementAsPath(self, word):
        """
        Take an element of the fundamental group and return a cyclically reduced path (list of edges) realizing the word.
        """
        tree = self.getSpanningTree()     # also converts None to 1
        tree.allow_multiple_edges(False)  # force it to have no multiple edges (implies unique labels)

        edges = self._graph.edges()

        converter = {}             # dictionary of generator to edge
        generators = []            # the edge-labels generate the group, these are svaed here
        for edge in self._graph.edges():
            if edge[2] == word.parent().one():
                continue
            generators.append(edge[2])
            converter.update({len(generators):edge})
        # write a word as tietze with the new generators
        tietze = self.convertGroupelementToHereTietze(word, gens=generators)
        tietze = self.cyclycallyReduced(tietze)     # cyclically reduce word

        number = tietze[-1]     # throw an error if the word was trivial
        # check orientation of last edge to get first vertex of cycle
        if number > 0:
            lastVertex = converter.get(abs(number))[1]
        else:
            lastVertex = converter.get(abs(number))[0]

        path = []
        for number in tietze:
            edge = converter.get(abs(number))
            if number > 0:
                start = edge[0]
                end = edge[1]
            else:
                start = edge[1]
                end = edge[0]
            # add path in tree
            treeWalk = tree.shortest_path(lastVertex, start)
            for i in range(len(treeWalk) - 1):
                if treeWalk[i] < treeWalk[i+1]:
                    path.append((treeWalk[i], treeWalk[i+1],
                                 tree.edge_label(treeWalk[i], treeWalk[i+1])))
                else:
                    path.append((treeWalk[i+1], treeWalk[i],
                                 tree.edge_label(treeWalk[i+1], treeWalk[i])))
            path.append(edge)
            # save lastVertex of path
            lastVertex = end
        return path

    def convertGroupelementToHereTietze(self, word, gens):
        """
        Take a word and write it in terms of the given generators.

        Currently nasty translation to GAP and back (soft ToDo).
        """
        # make it compatible for translation into GAP
        rank = len(gens)
        xTietze = list(word.Tietze())
        tgens = [list(gen.Tietze()) for gen in gens]

        # translate word and generators to GAP
        gap.eval('tWord := %s;; tgens := %s;; rank := %s;;'
                 % (xTietze, tgens, rank))
        gap.eval('G := FreeGroup(rank);; '
                 'Ggens := GeneratorsOfGroup(G);;')
        gap.eval('word := One(G);; for i in tWord do '
                 'nextLetter := Ggens[AbsInt(i)]; '
                 'if i>0 then word := word*nextLetter; '
                 'else word := word/nextLetter; fi;  od;')
        gap.eval('newGens := [];; '
                 'for tWord in tgens do newGen := One(G);; '
                 'for i in tWord do nextLetter := Ggens[AbsInt(i)]; '
                 'if i>0 then newGen := newGen*nextLetter; '
                 'else newGen := newGen/nextLetter; fi;  '
                 'od; Add(newGens, newGen); od;')
        gap.eval('H := Subgroup(G, newGens);;')

        # change the base in GAP
        gap.eval('hom := EpimorphismFromFreeGroup(H);;')
        gap.eval('newWord := PreImagesRepresentative(hom, word);;')

        # and translate new Tietze-word back to Sage
        newTietze = gap('TietzeWordAbstractWord(newWord);').sage()
        return newTietze


    def cyclycallyReduced(self, tietzeWord):
        """
        Take a Tietze represenation of a word and return a cyclycally reduced copy of it.

        Does only conjugate and not check interior of word.
        """
        tietzeList = list(tietzeWord)
        while tietzeList and tietzeList[0] == -tietzeList[-1]:
            tietzeList.pop()
            tietzeList.pop(0)
        newWord = tuple(tietzeList)
        return newWord

    # DISTANCES
    # Functions to determine the Lipschitz distance and lengths of paths.

    def lengthOfElement(self, groupElement):
        """
        Return length of the cyclycally reduced path corresponding to the element of the fundamental group.
        """
        return(self.lengthOfPath(self.groupelementAsPath(groupElement)))

    def lengthOfPath(self, path, weights=None, graph=None):
        """
        Take a list of edges (=path) and return the length, i.e. sum of edge weights.

        If path is empty or point, return 0.
        """
        if not weights:
            weights = self._weights
        if not graph:
            graph = self._graph
        if not path:
            return 0
        length = 0
        for edge in path:
            weight = weights.get(edge)
            length = length + weight
        return length

    def distanceTo(self, markedGraph, lazy=False):
        """
        Compute asymmetric Lipschitz distance to given markedGraph

        If lazy, it takes the saved candidates if possible.
        """
        distance = log(max(self.candFractions(markedGraph, lazy).values()
                          )*self.volume()/markedGraph.volume())
        return distance

    def volume(self):
        """
        Return the volume, i.e. the sum of edge lengths of this graph
        """
        edges = self._graph.edges()
        sum = self._weights.get(edges[0])
        for edge in edges:
            sum += self._weights.get(edge)
        sum -= self._weights.get(edges[0])
        # soft ToDo: same as in paths this could be nicer,
        # but like that we can plugin every abelian magma
        return sum


    def candFractions(self, markedGraph, lazy=False):
        """
        Return a dictionary of all ratios of lengths

        These are ratios of lenghts of the candidates here and their image in the given marked graph

        Keys are elements in fundamental group
        If lazy, it will not reevaluate the candidates (if there are at least some simple cycles).
        """
        candidates = self.candidates(reevaluate= (not lazy))
        candidates = candidates[0] + candidates[1] + candidates[2]
        fractionCand = {}
        for candidate in candidates:
            denominator = self.lengthOfElement(candidate)
            numerator = markedGraph.lengthOfElement(candidate)
            fractionCand.update({candidate: numerator/denominator})
        return fractionCand
